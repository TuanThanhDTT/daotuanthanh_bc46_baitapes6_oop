let listPerson = new ListPerson();


//Hàm thêm Student

 function addStudent(){
     let student = layThongTinTuFormStudent();
     
     //Kiểm tra mã Student
     let isValid = kiemTraNull(student.ma, "tbMaStudent", "Tài khoản không được để trống!") && kiemTraSo(student.ma, "tbMaStudent") && kiemTraDoDai(1,4, "tbMaStudent",student.ma, "Mã từ 1 đến 4 kí tự!") && kiemTraTrung(student.ma, listPerson.persons, "tbMaStudent");
     //Kiểm tra họ tên
    isValid &= kiemTraTen(student.hoTen, "tbHoTenStudent", "Tên không hợp lệ!") && kiemTraNull(student.hoTen, "tbHoTenStudent", "Tên không được để trống!");
    
    //Kiểm tra địa chỉ
    isValid &= kiemTraNull(student.diaChi, "tbDiaChiStudent", "Địa chỉ không được để trống!")

    //Kiểm tra email
    isValid &= kiemTraNull(student.email, "tbEmailStudent", "Email không được để trống!") && kiemTraEmail(student.email, "tbEmailStudent");
    
    //Kiểm tra điểm toán
    isValid &= kiemTraNull(student.toan,"diemToanSpan","Chưa nhập điểm Toán!") && kiemTraDiem(student.toan, "diemToanSpan", "Vui lòng nhập điểm chỉ từ 0 đến 10");

    //Kiểm tra điểm lý
    isValid &= kiemTraNull(student.ly,"diemLySpan","Chưa nhập điểm Lý!") && kiemTraDiem(student.ly, "diemLySpan", "Vui lòng nhập điểm chỉ từ 0 đến 10");
    
    //Kiểm tra điểm hóa
    isValid &= kiemTraNull(student.hoa,"diemHoaSpan","Chưa nhập điểm Hóa!") && kiemTraDiem(student.hoa, "diemHoaSpan", "Vui lòng nhập điểm chỉ từ 0 đến 10");

    if(isValid)
    {
        listPerson.addPerson(student);
        renderListPerson(listPerson.persons);
        $("#myModal").modal("hide");
        document.getElementById("QLStudent").reset();
    }
 }
  
  //Hàm thêm Employee
 function addEmployee(){
    let employee = layThongTinTuFormEmployee();

     //Kiểm tra mã Student
     let isValid = kiemTraNull(employee.ma, "tbMaEmployee", "Tài khoản không được để trống!") && kiemTraSo(employee.ma, "tbMaEmployee") && kiemTraDoDai(1,4, "tbMaEmployee",employee.ma, "Mã từ 1 đến 4 kí tự!") && kiemTraTrung(employee.ma, listPerson.persons, "tbMaEmployee");
     //Kiểm tra họ tên
    isValid &= kiemTraTen(employee.hoTen, "tbHoTenEmployee", "Tên không hợp lệ!") && kiemTraNull(employee.hoTen, "tbHoTenEmployee", "Tên không được để trống!");
    
    //Kiểm tra địa chỉ
    isValid &= kiemTraNull(employee.diaChi, "tbDiaChiEmployee", "Địa chỉ không được để trống!");

    //Kiểm tra email
    isValid &= kiemTraNull(employee.email, "tbEmailEmployee", "Email không được để trống!") && kiemTraEmail(employee.email, "tbEmailEmployee");

    //Kiểm tra số ngày làm việc
    isValid &= kiemTraNull(employee.soNgayLV, "tbsoNgayLV", "Số ngày làm việc không được để trống!") && kiemTraSo(employee.soNgayLV, "tbsoNgayLV");
    
    //Kiểm tra lương ngày
    isValid &= kiemTraNull(employee.luongNgay, "tbLuongNgay", "Số ngày làm việc không được để trống!") && kiemTraSo(employee.luongNgay, "tbLuongNgay") && kiemTraluongCB(employee.luongNgay,"tbLuongNgay");

    if(isValid){
        listPerson.addPerson(employee);
        renderListPerson(listPerson.persons);
        $("#myModalEmployee").modal("hide");
        document.getElementById("QLEmployee").reset();
    }

 }
 
 //Hàm thêm Customer
 function addCustomer(){
    let customer = layThongTinTuFormCustomer();

     //Kiểm tra mã Student
     let isValid = kiemTraNull(customer.ma, "tbMaCustomer", "Tài khoản không được để trống!") && kiemTraSo(customer.ma, "tbMaCustomer") && kiemTraDoDai(1,4, "tbMaCustomer",customer.ma, "Mã từ 1 đến 4 kí tự!") && kiemTraTrung(customer.ma, listPerson.persons, "tbMaCustomer");
     //Kiểm tra họ tên
    isValid &= kiemTraTen(customer.hoTen, "tbHoTenCustomer", "Tên không hợp lệ!") && kiemTraNull(customer.hoTen, "tbHoTenCustomer", "Tên không được để trống!");
    
    //Kiểm tra địa chỉ
    isValid &= kiemTraNull(customer.diaChi, "tbDiaChiCustomer", "Địa chỉ không được để trống!")

    //Kiểm tra email
    isValid &= kiemTraNull(customer.email, "tbEmailCustomer", "Email không được để trống!") && kiemTraEmail(customer.email, "tbEmailCustomer");

    //Kiểm tra tên công ty
    isValid &= kiemTraNull(customer.tenCongTy, "tbTenCongTy", "Tên công ty trống!");

    //Kiểm tra hóa đơn
    isValid &= kiemTraNull(customer.hoaDon, "tbHoaDon", "Hóa đơn trống!") && kiemTraSo(customer.hoaDon, "tbHoaDon");

    //Kiểm tra đánh giá
    isValid &= kiemTraNull(customer.danhGia, "tbDanhGia", "Đánh giá trống!");
   if(isValid){
    listPerson.addPerson(customer);
    renderListPerson(listPerson.persons);
    $("#myModalCustomer").modal("hide");
    document.getElementById("QLCustomer").reset();
   }
 }

 //Lọc danh sách theo type Student
 document.getElementById("inputGroupSelect01").addEventListener("click",function filterListPerson(){
    let typePerson = document.getElementById("inputGroupSelect01").value;
    if(typePerson != "All"){
        let filterPerson = listPerson.persons.filter(function(item_array){
            return item_array.type == typePerson;
        });
        renderListPerson(filterPerson);
    } else {
        renderListPerson(listPerson.persons);
    }
  
})


//Sắp xếp danh sách theo họ
document.getElementById("btnSapXep").addEventListener("click", function sortListPerson() {
    listPerson.persons.sort((a, b) => {
        const lastNameA = a.hoTen.split(' ')[0]; // Lấy họ của đối tượng A
        const lastNameB = b.hoTen.split(' ')[0]; // Lấy họ của đối tượng B
        return lastNameA.localeCompare(lastNameB);
    });
    
    console.log(listPerson.persons);
    renderListPerson(listPerson.persons);
});

//Hàm delete cho person Student
function deleteStudent(maStudent){
    let index_item = [];
    for(let i = 0; i < listPerson.persons.length; i++){
        if(listPerson.persons[i].ma == maStudent){
            index_item.push(i);
        };
      };

    for ( i = index_item.length - 1; i >= 0; i--) {
        listPerson.persons.splice(index_item[i],1);
        renderListPerson(listPerson.persons);
      };
    
}

//Hàm delete cho person Employee
function deleteEmployee(maEmployee){
    let index_item = [];
    for(let i = 0; i < listPerson.persons.length; i++){
        if(listPerson.persons[i].ma == maEmployee){
            index_item.push(i);
        };
      };

    for ( i = index_item.length - 1; i >= 0; i--) {
        listPerson.persons.splice(index_item[i],1);
        renderListPerson(listPerson.persons);
      };
}

//Hàm delete cho person Customer
function deleteCustomer(maCustomer){
    let index_item = [];
    for(let i = 0; i < listPerson.persons.length; i++){
        if(listPerson.persons[i].ma == maCustomer){
            index_item.push(i);
        };
      };

    for ( i = index_item.length - 1; i >= 0; i--) {
        listPerson.persons.splice(index_item[i],1);
        renderListPerson(listPerson.persons);
      };
}

//Hàm setting và update cho Student
function settingStudent(maStudent){
    document.getElementById("maStudent").disabled = true;
    document.getElementById("btnCapNhatStudent").style.display = "inline-block";
    document.getElementById("btnThemStudent").style.display = "none";
    //Sử dụng findIndex để lấy index và Splice để xóa
    //Tìm index của phần tử có tài khoản trùng với tham số
    let index = listPerson.persons.findIndex(function(item){
        return item.ma == maStudent;
    });
    showThongTinLenFormStudent(listPerson.persons[index]);
}
function updateStudent(){
    let update = layThongTinTuFormStudent();
    let index = listPerson.persons.findIndex(function(item){
        return item.ma == update.ma;
    });

    listPerson.persons[index] = new Student(update.ma, update.hoTen, update.diaChi, update.email, update.toan, update.ly, update.hoa, update.type);
    //Dùng splice cập nhật lại thông tin tại phần tử có index cùng mã
    listPerson.persons.splice(index,1,listPerson.persons[index]);

    renderListPerson(listPerson.persons);
    $("#myModal").modal("hide");
    document.getElementById("maStudent").disabled = false;
    document.getElementById("btnThemStudent").style.display = "inline-block";
    document.getElementById("QLStudent").reset();
}

//Hàm setting và update cho Employee
function settingEmployee(maEmployee){
    document.getElementById("maEmployee").disabled = true;
    document.getElementById("btnCapNhatEmployee").style.display = "inline-block";
    document.getElementById("btnThemEmployee").style.display = "none";
    //Sử dụng findIndex để lấy index và Splice để xóa
    //Tìm index của phần tử có tài khoản trùng với tham số
    let index = listPerson.persons.findIndex(function(item){
        return item.ma == maEmployee;
    });
    console.log("index",index);
    console.log(listPerson.persons[index]);
    console.log("listPerson.persons",listPerson.persons);
    showThongTinLenFormEmployee(listPerson.persons[index]);
}
function updateEmployee(){
    let update = layThongTinTuFormEmployee();
    let index = listPerson.persons.findIndex(function(item){
        return item.ma == update.ma;
    });

    listPerson.persons[index] = new Employee(update.ma, update.hoTen, update.diaChi, update.email, update.soNgayLV, update.luongNgay, update.type);
    //Dùng splice cập nhật lại thông tin tại phần tử có index cùng mã
    listPerson.persons.splice(index,1,listPerson.persons[index]);

    renderListPerson(listPerson.persons);
    $("#myModalEmployee").modal("hide");
    document.getElementById("maEmployee").disabled = false;
    document.getElementById("btnThemEmployee").style.display = "inline-block";
    document.getElementById("QLEmployee").reset();
}

//Hàm setting và update cho Customer
function settingCustomer(maCustomer){
    document.getElementById("maCustomer").disabled = true;
    document.getElementById("btnCapNhatCustomer").style.display = "inline-block";
    document.getElementById("btnThemCustomer").style.display = "none";
    //Sử dụng findIndex để lấy index và Splice để xóa
    //Tìm index của phần tử có tài khoản trùng với tham số
    let index = listPerson.persons.findIndex(function(item){
        return item.ma == maCustomer;
    });
    console.log("index",index);
    console.log(listPerson.persons[index]);
    console.log("listPerson.persons",listPerson.persons);
    showThongTinLenFormCustomer(listPerson.persons[index]);
}
function updateCustomer(){
    let update = layThongTinTuFormCustomer();
    let index = listPerson.persons.findIndex(function(item){
        return item.ma == update.ma;
    });

    listPerson.persons[index] = new Customer(update.ma, update.hoTen, update.diaChi, update.email, update.tenCongTy, update.hoaDon, update.danhGia, update.type);
    //Dùng splice cập nhật lại thông tin tại phần tử có index cùng mã
    listPerson.persons.splice(index,1,listPerson.persons[index]);

    renderListPerson(listPerson.persons);
    $("#myModalCustomer").modal("hide");
    document.getElementById("maCustomer").disabled = false;
    document.getElementById("btnThemCustomer").style.display = "inline-block";
    document.getElementById("QLCustomer").reset();
}


//Khi bấm button addStudent
document.getElementById("btnThemStudent1").addEventListener("click", function(){
    document.getElementById("btnCapNhatStudent").style.display = "none";
    document.getElementById("btnThemStudent").style.display = "inline-block";
});

//Khi bấm button addEmployee
document.getElementById("btnThemEmployee1").addEventListener("click", function(){
    document.getElementById("btnCapNhatEmployee").style.display = "none";
    document.getElementById("btnThemEmployee").style.display = "inline-block";
});

//Khi bấm button addCustomer
document.getElementById("btnThemCustomer1").addEventListener("click", function(){
    document.getElementById("btnCapNhatCustomer").style.display = "none";
    document.getElementById("btnThemCustomer").style.display = "inline-block";
});

document.getElementById("btnDong").addEventListener("click",function btnDong(){
    document.getElementById("maStudent").disabled = false;
    document.getElementById("QLStudent").reset();
});

document.getElementById("btnDong2").addEventListener("click",function btnDong2(){
    document.getElementById("maEmployee").disabled = false;
    document.getElementById("QLEmployee").reset();
});

document.getElementById("btnDong3").addEventListener("click",function btnDong3(){
    document.getElementById("maCustomer").disabled = false;
    document.getElementById("QLCustomer").reset();
});





